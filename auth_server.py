#!/usr/bin/env python

import MySQLdb, json, ConfigParser, logging
from cgi import parse_qs, escape
from phpbb.auth.sql import setup
from phpbb.auth.auth_db import login_db
from wsgiref.simple_server import make_server

#
# This is needed because relative paths in WSGI are weird and I don't know
# how to get this info in any other way.
#
wsgi_config_file = '/home/wonko/projects/phpbb_auth/auth_server.cfg'

class AuthServer:
	config = ConfigParser.RawConfigParser()

	def __init__(self, config_file):
		if not config_file:
			raise Exception("config_file argument required!")

		self.config.read(config_file)

		# create logger
		self.logger = logging.getLogger(__name__)
		self.logger.setLevel(logging.DEBUG)
		# create formatter and add it to the handlers
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

		try:
			# create file handler which logs even debug messages
			fh = logging.FileHandler(self.config.get('server', 'logfile'))
			fh.setLevel(logging.DEBUG)
			fh.setFormatter(formatter)
			self.logger.addHandler(fh)
		except:
			pass

		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logging.DEBUG)
		ch.setFormatter(formatter)
		# add the handlers to the logger
		self.logger.addHandler(ch)

		self.logger.debug('Starting Up!')

		self.conn = MySQLdb.connect(
			host = self.config.get('database', 'host'),
			db = self.config.get('database', 'db'),
			user = self.config.get('database', 'user'),
			passwd = self.config.get('database', 'passwd')
		)
		setup(self.conn)

	def authcode_valid(self, authcode):
		for code, v in self.config.items('authcodes'):
			if code == authcode:
				return True

		return False

	def auth_server(self, environ, start_response):
		response = dict()

		# the environment variable CONTENT_LENGTH may be empty or missing
		try:
			request_body_size = int(environ.get('CONTENT_LENGTH', 0))
		except (ValueError):
			request_body_size = 0

		# When the method is POST the query string will be sent
		# in the HTTP request body which is passed by the WSGI server
		# in the file like wsgi.input environment variable.
		request_body = environ['wsgi.input'].read(request_body_size)
		d = parse_qs(request_body)

		username = d.get('username', [''])[0] # Returns the first username value. (there should be only one)
		password = d.get('password', [''])[0] # Returns the first password value. (there should be only one)
		authcode = d.get('authcode', [''])[0] # Returns the first authcode value. (there should be only one)

		# Always escape user input to avoid script injection
		username = escape(username)
		password = escape(password)
		authcode = escape(authcode)

		result, user_row = login_db(username, password)

		if not self.authcode_valid(authcode):
			start_response('200 OK', [('Content-Type', 'text/plain')])
			return json.dumps({'authenticated': False, 'reason': 'Bad Authcode'})

		if result == "LOGIN_SUCCESS":
			response['authenticated'] = True
			response['user_email'] = user_row['user_email']

			cursor = self.conn.cursor()

			cursor.execute("""SELECT eveapi_keyid, eveapi_vcode FROM phpbb_users WHERE user_id = %s""", (user_row['user_id'],))
			eve_api = cursor.fetchone()
			response['eveapi_keyid'] = eve_api[0]
			response['eveapi_vcode'] = eve_api[1]

			cursor.execute("""SELECT group_name FROM phpbb_groups INNER JOIN phpbb_user_group ON phpbb_groups.group_id = phpbb_user_group.group_id WHERE phpbb_user_group.user_id = %s""", (user_row['user_id'],))
			group_list = cursor.fetchall()

			groups = list()
			for group in group_list:
				groups.append(group[0])

			response['groups'] = groups

			cursor.close()
		else:
			response['authenticated'] = False
			response['reason'] = "Bad Bassword"

		self.logger.debug(json.dumps(response))
		start_response('200 OK', [('Content-Type', 'text/plain')])

		if not environ['PATH_INFO'] or environ['PATH_INFO'] == "/":
			return json.dumps(response)

		if environ['PATH_INFO'].startswith("/json"):
			return json.dumps(response)

	def run_server(self):
		#print(config.get('server', 'interface'))
		httpd = make_server('', self.config.getint('server', 'port'), self.auth_server)
		self.logger.info("""Serving HTTP on port {0}...""".format(self.config.getint('server', 'port')))

		# Respond to requests until process is killed
		httpd.serve_forever()

def application(environ, start_response):
		phpbb_auth = AuthServer(wsgi_config_file)
		output = phpbb_auth.auth_server(environ, start_response)
		return output
