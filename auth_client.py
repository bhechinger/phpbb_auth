#!/usr/bin/env python

import urllib, urllib2
from argparse import ArgumentParser

parser = ArgumentParser(description="Authenticate phpBB user")
parser.add_argument("-u", "--username", dest="username", required=True,
	help="USERNAME to authenticate as", metavar="USERNAME")
parser.add_argument("-p", "--password", dest="password", required=True,
	help="PASSWORD to authenticate with", metavar="PASSWORD")
parser.add_argument("-U", "--url", dest="url", required=True,
	help="URL of auth server", metavar="URL")
parser.add_argument("-a", "--authcode", dest="authcode", required=True,
	help="AUTHCODE designated by server", metavar="AUTHCODE")

args = parser.parse_args()

values = {'username': args.username, 'password': args.password, 'authcode': args.authcode}

data = urllib.urlencode(values)
req = urllib2.Request(args.url, data)

try:
	response = urllib2.urlopen(req)
	the_page = response.read()
	print(the_page)
except urllib2.HTTPError as e:
	print(e)
