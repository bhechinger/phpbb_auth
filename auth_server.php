<?php
/**
*
* @package phpbb_auth
* @version $Id$
* @copyright (c) 2013 Brian Hechinger
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/*
 * I'm too lazy to do this in a config file because I'd *really* like to make
 * this a MOD anyway and store this in a DB so this hacky shit is here for now.
 */

$api_keys = array(
	'auth1', // auth code 1
	'auth2', // auth code 2
	'auth3'  // auth code 3
);

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
//include($phpbb_root_path . 'includes/functions_display.' . $phpEx);

// Not sure I want to do this
// Start session management
$user->session_begin();
$auth->acl($user->data);

$username = $_POST[username];
$password = $_POST[password];
$api_key = $_POST[api_key];

if (in_array($api_key, $api_keys)) {
	$a = $auth->login($username, $password, False, 1, 0);
	if ($a[status] != 3) {
		$response[authenticated] = False;
		$response[reason] = $a[error_msg];
	} else {
		$user->setup();

		/* Data to return:
			response['groups'] = groups
		*/

		$response[authenticated] = True;
		$response[user_id] = $user->data[user_id];
		$response[user_email] = $user->data[user_email];
		$response[eveapi_keyid] = $user->data[eveapi_keyid];
		$response[eveapi_vcode] = $user->data[eveapi_vcode];

		$sql = "SELECT group_name FROM phpbb_groups INNER JOIN phpbb_user_group ON phpbb_groups.group_id = phpbb_user_group.group_id WHERE phpbb_user_group.user_id = " . $user->data[user_id];
		$result = $db->sql_query($sql);
		foreach ($db->sql_fetchrowset($result) as $row) {
			$response[groups][] = $row[group_name];
		}
		$db->sql_freeresult($result);

		//$response[all] = $user->data;
	}
} else {
	$response[authenticated] = False;
	$response[reason] = "Invalid auth code";
}

print(json_encode($response));

$user->session_kill();
$user->session_begin();
?>
